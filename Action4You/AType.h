//
//  AType.h
//  Action4You
//
//  Created by Dominik Koscica on 18/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

typedef enum{
    HUMANITARIAN = 1,
    EDUCATION,
    WORK,
    SHARE
} AType;
