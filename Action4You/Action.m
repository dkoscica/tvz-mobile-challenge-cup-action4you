//
//  Action.m
//  Action4You
//
//  Created by Bruno Grizli on 4/11/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "Action.h"
#import "Parse/PFObject+Subclass.h"

@implementation Action

@dynamic name;
@dynamic actionDesc;
@dynamic organizer;
@dynamic dateFrom;
@dynamic dateTo;
@dynamic actionLocation;
@dynamic locationLatitude;
@dynamic locationLongitude;
@dynamic type;
@dynamic participantCount;
@dynamic country;
@dynamic town;

#pragma mark - Init methods

- (instancetype) initActionWithName: (NSMutableString *)name
                        description: (NSMutableString *)description
                          organizer: (Member *)member
                           dateFrom: (NSDate *)dateFrom
                             dateTo: (NSDate *) dateTo
                     actionLocation: (NSMutableString *) actionLocation
                  locactionLatitude: (double) locLat
                  locationLongitude: (double) locLng
                         actionType: (ActionTypes *) actionType
                   participantCount: (int) count
                            country: (NSMutableString *) country
                               town: (NSMutableString *) town{
    
    self = [self init];
    if(self) {
        self.name = name;
        self.actionDesc = description;
        self.organizer = member;
        self.dateFrom = dateFrom;
        self.dateTo = dateTo;
        self.actionLocation = actionLocation;
        self.locationLatitude = locLat;
        self.locationLongitude = locLng;
        self.type = actionType;
        self.participantCount = count;
        self.country = country;
        self.town = town;
    }
    return self;
}

+ (void) load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return @"Action";
}
    
@end
