//
//  NSString+FormValidation.h
//  Action4You
//
//  Created by Dominik Koscica on 26/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (FormValidation)

- (BOOL)isValidName;
- (BOOL)isValidInformation;
- (BOOL)isValidStartDateAndTime;
- (BOOL)isValidEndDateAndTime;
- (BOOL)isValidAddress;
- (BOOL)isValidTelephone;
- (BOOL)isValidEmail;
- (BOOL)isValidUsername;
- (BOOL)isValidPassword;

@end
