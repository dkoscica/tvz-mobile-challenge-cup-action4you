//
//  NSString+FormValidation.m
//  Action4You
//
//  Created by Dominik Koscica on 26/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "NSString+FormValidation.h"

@implementation NSString (FormValidation)

static int const MINIMAL_STRING_LENGTH = 4;

- (BOOL)isValidName {
    return (self.length >= MINIMAL_STRING_LENGTH);
}

- (BOOL)isValidInformation {
    if ([self isEqualToString:NSLocalizedString(@"ACTION_INFORMATION", "")] || [self isEqualToString:NSLocalizedString(@"INFORMATION_ERROR", "")]){
        return NO;
    }
    return YES;
}

- (BOOL)isValidStartDateAndTime {
    if ([self isEqualToString:NSLocalizedString(@"START_DATE_AND_TIME", "")] || [self isEqualToString:NSLocalizedString(@"START_DATE_ERROR", "")]){
        return NO;
    }
    return YES;
}

- (BOOL)isValidEndDateAndTime {
    if ([self isEqualToString:NSLocalizedString(@"END_DATE_AND_TIME", "")] || [self isEqualToString:NSLocalizedString(@"END_DATE_ERROR", "")]){
        return NO;
    }
    return YES;
}

- (BOOL)isValidAddress {
    if ([self isEqualToString:NSLocalizedString(@"ACTION_LOCATION", "")] || [self isEqualToString:NSLocalizedString(@"ADDRESS_ERROR", "")]){
        return NO;
    }
    return YES;
}

- (BOOL)isValidTelephone {
    //NSString *phoneRegex = @"^\\+(?:[0-9] ?){6,14}[0-9]$";
    //NSPredicate *phonePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    //return [phonePredicate evaluateWithObject:self];
    return (self.length >= MINIMAL_STRING_LENGTH);
}

- (BOOL)isValidEmail {
    NSString *emailRegex = @"[^@]+@[A-Za-z0-9.-]+\\.[A-Za-z]+";
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailPredicate evaluateWithObject:self];
}

- (BOOL)isValidUsername {
    return (self.length >= MINIMAL_STRING_LENGTH);
}

- (BOOL)isValidPassword {
    return (self.length >= MINIMAL_STRING_LENGTH);
}

@end
