//
//  UIColor+Action4YouColors.h
//  Action4You
//
//  Created by Dominik Koscica on 19/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Action4YouColors)

+ (UIColor *) action4YouPurpleColor;
+ (UIColor *) action4YouDarkPurpleColor;
+ (UIColor *) action4YouTransparentGrayColor;
+ (UIColor *) action4YouGreenColor;
+ (UIColor *) action4YouBlueColor;
+ (UIColor *) action4YouRedColor;
+ (UIColor *)action4YouYellowColor;

@end
