//
//  UIColor+Action4YouColors.m
//  Action4You
//
//  Created by Dominik Koscica on 19/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "UIColor+Action4YouColors.h"

@implementation UIColor (Action4YouColors)

+ (UIColor *)action4YouPurpleColor {
    return [UIColor colorWithRed:132.0/255 green:109./255 blue:188.0/255 alpha:1.0];
}

+ (UIColor *)action4YouDarkPurpleColor {
    return [UIColor colorWithRed:46.0/255 green:36.0/255 blue:71.0/255 alpha:0.2];
}

+ (UIColor *)action4YouTransparentGrayColor {
    return [UIColor colorWithRed:71.0/255 green:69.0/255 blue:78.0/255 alpha:0.5];
}

+ (UIColor *)action4YouGreenColor {
    return [UIColor colorWithRed:76.0/255 green:217.0/255 blue:100.0/255 alpha:1.0];
}

+ (UIColor *)action4YouBlueColor {
    return [UIColor colorWithRed:52.0/255 green:170.0/255 blue:220.0/255 alpha:1.0];
}

+ (UIColor *)action4YouRedColor {
    return [UIColor colorWithRed:255.0/255 green:45.0/255 blue:85.0/255 alpha:1.0];
}

+ (UIColor *)action4YouYellowColor {
    return [UIColor colorWithRed:255.0/255 green:204.0/255 blue:0.0/255 alpha:1.0];
}

@end
