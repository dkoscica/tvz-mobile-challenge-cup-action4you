//
//  NavigationViewController.h
//  Action4You
//
//  Created by Dominik Košćica on 11/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Action4YouNavigationViewController : UINavigationController

- (void)setupNavigationBar;
- (void)addNavigationBarBlurEffect;
- (void)changeNavigationBarColor:(UIColor *) color;

@end
