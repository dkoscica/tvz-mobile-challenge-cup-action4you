//
//  NavigationViewController.m
//  Action4You
//
//  Created by Dominik Košćica on 11/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "Action4YouNavigationViewController.h"
#import "UIColor+Action4YouColors.h"

@interface Action4YouNavigationViewController ()

@end

@implementation Action4YouNavigationViewController

#
# pragma mark - Lifecycle methods
#
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigationBar];
}

- (void) viewWillAppear:(BOOL)animated {
}

- (void) viewDidAppear:(BOOL)animated {

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#
# pragma mark - Helper methods
#
- (void)setupNavigationBar {
    self.navigationBar.translucent = true;
    self.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationBar.backgroundColor = [UIColor action4YouPurpleColor];

    //Remove back controller text
    UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] init];
    backBarButton.title = @"";
    self.navigationBar.topItem.backBarButtonItem = backBarButton;
    
    [self addNavigationBarBlurEffect];
}

- (void)addNavigationBarBlurEffect {
    CGRect bounds = self.navigationBar.bounds;
    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
    visualEffectView.frame = bounds;
    visualEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    //Add blur to view
    
    //Navigation back stops working if this is enabled!!!
    //[self.navigationBar addSubview:visualEffectView];
    [self.navigationBar sendSubviewToBack:visualEffectView];
}

- (void)changeNavigationBarColor: (UIColor *) color {
    self.navigationBar.backgroundColor = color;
    self.navigationBar.alpha = 0.9;
}

@end
