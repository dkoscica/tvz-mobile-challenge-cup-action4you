//
//  MapViewContoller.h
//  TVZ-Mc2-App
//
//  Created by Dominik Koscica on 15/03/15.
//  Copyright (c) 2015 Dominik Koscica. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <GoogleMaps/GoogleMaps.h>

@interface ActionsMapViewContoller : BaseViewController

@property(nonatomic, strong)id delegate;
@property (nonatomic) ActionsMapViewControllerMode mode;

@property (nonatomic, strong) Action *action;
@property (nonatomic, strong) NSMutableArray *actionsArray;

@property (nonatomic, strong) IBOutlet GMSMapView *map;

@end
