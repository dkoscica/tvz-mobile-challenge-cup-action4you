//
//  MapViewContoller.m
//  TVZ-Mc2-App
//
//  Created by Dominik Koscica on 15/03/15.
//  Copyright (c) 2015 Dominik Koscica. All rights reserved.
//

#import "ActionsMapViewContoller.h"
#import "SendActionProtocol.h"

@interface ActionsMapViewContoller () <GMSMapViewDelegate, CLLocationManagerDelegate, UITabBarDelegate, SendActionProtocol>

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) GMSMarker *marker;

@property (nonatomic, strong) IBOutlet UITabBar *tabBar;
@property (nonatomic, strong) IBOutlet UIView *infoView;

@property (nonatomic, strong) NSMutableString *actionLocation;
@property (nonatomic, strong) NSMutableSet *actionMarkersFromCacheOrParse;
@property (nonatomic, strong) NSMutableSet *filteredMarkers;

@end

@implementation ActionsMapViewContoller

#
# pragma mark - Lifecycle methods
#
-(void)viewDidLoad {
    [super viewDidLoad];
    [self setupGoogleMaps];
    [self setupLocationManager];
    
    self.actionLocation = [NSMutableString new];
    [self generateMarkersFromCacheOrParse];
}

- (void)viewWillAppear:(BOOL)animated {
    [self setupController];
    [self setupTabBar];
}

//Delegate method called
-(void)viewWillDisappear:(BOOL)animated {
    [self.delegate sendActionLocation:self.actionLocation : self.marker.layer.latitude : self.marker.layer.longitude];
}

#
# pragma mark - Google maps methods
#
- (void)setupGoogleMaps {
    
    self.map = [GMSMapView mapWithFrame:self.view.bounds camera:nil];
    self.map.settings.compassButton = YES;
    self.map.settings.zoomGestures = YES;
    self.map.settings.myLocationButton = YES;
    self.map.delegate = self;

    [self.view addSubview: self.map];
    [self.view addSubview: self.tabBar];
    [self.view addSubview: self.infoView];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.map.padding = UIEdgeInsetsMake(self.topLayoutGuide.length + 95, 0, self.bottomLayoutGuide.length + 55, 0);
}

- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker{
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(marker.layer.latitude, marker.layer.longitude);
    [self useGeocoderToUpdateMarker:location];
}

- (void)useGeocoderToUpdateMarker: (CLLocationCoordinate2D)coordinate {
    GMSGeocoder *geocoder = [GMSGeocoder geocoder];
    [geocoder reverseGeocodeCoordinate:coordinate completionHandler:^(GMSReverseGeocodeResponse *response, NSError *error) {
        
        NSMutableString *address = [response.firstResult.thoroughfare mutableCopy];
        NSMutableString *town = [response.firstResult.locality mutableCopy];
        
        self.marker.position = coordinate;
        self.marker.title = address;
        self.marker.snippet = town;
        
        [self.actionLocation setString:@""];
        [self.actionLocation appendString:address];
        [self.actionLocation appendString:@"-"];
        [self.actionLocation appendString:town];
        
        [self.map setSelectedMarker:self.marker];
    }];
}

#
# pragma mark - Markers
#
- (void)createMarkerFromCurrentLocation {
    // Creates a marker in the center of the map.
    self.marker = [[GMSMarker alloc] init];
    self.marker.appearAnimation = kGMSMarkerAnimationPop;
    self.marker.icon = [self getMapMarkerIconFromAction:self.action.actionTypeInt];
    self.marker.draggable = YES;
    self.marker.map = self.map;
}

- (void)createMarkerFromAction: (Action*)action {
    
    CLLocationCoordinate2D markerLocation = CLLocationCoordinate2DMake(action.locationLatitude, action.locationLongitude);
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:markerLocation zoom:ZOOM_LEVEL];
    
    // Creates a marker in the center of the map.
    GMSMarker *marker = [GMSMarker new];
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.position = markerLocation;
    marker.title = action.name;
    marker.snippet = action.actionLocation;
    marker.icon = [self getMapMarkerIconFromAction:action.actionTypeInt];
    marker.map = self.map;
    
    [self.map setSelectedMarker:marker];
    [self.map animateToCameraPosition:camera];
}

- (void)generateMarkersFromCacheOrParse {
    
    //Populate set from actionsArray
    self.actionMarkersFromCacheOrParse = [NSMutableSet setWithArray:self.actionsArray];
    for(Action *action in self.actionMarkersFromCacheOrParse) {
        if(action != nil && action.locationLatitude != 0.0 && action.locationLongitude != 0.0) {
            [self createMarkerFromAction:action];
        }
    }
}

- (void)filterMarkersByActionType: (int)type {
    
    //Populate set from actionsArray
    self.filteredMarkers = [NSMutableSet new];
    for(Action *action in self.actionMarkersFromCacheOrParse) {
        if(action != nil && action.locationLatitude != 0.0 && action.locationLongitude != 0.0) {
            
            if(action.actionTypeInt == type) {
                [self.filteredMarkers addObject:action];
            }
        }
    }
    
    [self.map clear];
    //Display filtered markers
    if([self.filteredMarkers count] > 0) {
        for(Action *action in self.filteredMarkers) {
            [self createMarkerFromAction:action];
        }
    } else {
        for(Action *action in self.actionMarkersFromCacheOrParse) {
            [self createMarkerFromAction:action];
        }
    }
}

#
# pragma mark - LocationManager
#
- (void)setupLocationManager {
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    self.locationManager.distanceFilter= 500;

    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager startMonitoringSignificantLocationChanges];
    [self.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    CLLocation *currentLocation = [locations lastObject];
    CLLocationCoordinate2D currentLocationCoordinates = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude);
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:currentLocationCoordinates zoom:ZOOM_LEVEL];
    
    NSDate* eventDate = currentLocation.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (abs(howRecent) < 15.0) {
        
        if(self.mode == MAP_ADD_EDIT_MODE){
            [self useGeocoderToUpdateMarker:currentLocationCoordinates];
            [self createMarkerFromCurrentLocation];
            [self.map setCamera:camera];
        } else {
            [self createMarkerFromAction:self.action];
        }
        
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"%@", error.localizedDescription);
}

#
# pragma mark - IBAction methods
#
- (IBAction)navigationBarSaveButton:(id)sender {
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    
    switch(item.tag) {
            
        case 1:
            [self filterMarkersByActionType: 1];
            break;
            
        case 2:
            [self filterMarkersByActionType: 2];
            break;
            
        case 3:
            [self filterMarkersByActionType: 3];
            break;
            
        case 4:
            [self filterMarkersByActionType: 4];
            break;
            
        case 5:
            [self filterMarkersByActionType: 5];
            break;
    }
}

#
# pragma mark - Helper methods
#
- (UIImage *)getMapMarkerIconFromAction:(int)actionType {
    
    switch(actionType) {
        case 1: return [UIImage imageNamed:MAP_MARKER_HUMANITARIAN];
        case 2: return [UIImage imageNamed:MAP_MARKER_EDUCATION];
        case 3: return [UIImage imageNamed:MAP_MARKER_WORK];
        case 4: return [UIImage imageNamed:MAP_MARKER_SHARE];
        default:
            return [UIImage imageNamed:MAP_MARKER_HUMANITARIAN];
    }
}

- (void)setupController {
    self.title = NSLocalizedString(@"ACTIONS_4_YOU_MAP_VIEW_CONTROLLER_TITLE", "");
    
    //Add/Edit mode from ActionsViewController
    if(self.mode == MAP_VIEW_MODE){
        self.tabBar.hidden = NO;
        self.infoView.hidden = YES;
    } else if (self.mode == MAP_ADD_EDIT_MODE) {
        self.tabBar.hidden = YES;
        self.infoView.hidden = NO;
        self.marker.draggable = YES;
    }
    
    //Remove back controller text
    UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] init];
    backBarButton.title = @"";
    self.navigationController.navigationBar.topItem.backBarButtonItem = backBarButton;
}

- (void)setupTabBar {
    
    [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.0]];
    
    //Unselected - default
    //[[UITabBarItem appearance] setImage: [[[UIImage imageNamed:@"action4you_people_icon.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    //[[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]} forState:UIControlStateNormal];
    
    //Selected
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]} forState:UIControlStateSelected];
}

@end
