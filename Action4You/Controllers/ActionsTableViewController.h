//
//  ActionsTableViewController.h
//  Action4You
//
//  Created by Dominik Koscica on 07/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Action4YouNavigationViewController.h"

@interface ActionsTableViewController : UITableViewController

@property (nonatomic, weak) IBOutlet UIBarButtonItem *userActionButton;

@property(nonatomic, strong) Action4YouNavigationViewController *action4YouNavigationViewController;

@end
