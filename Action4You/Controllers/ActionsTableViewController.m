//
//  ActionsTableViewController.m
//  Action4You
//
//  Created by Dominik Koscica on 07/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "ActionsTableViewController.h"
#import "ActionsMapViewContoller.h"
#import "ActionTableViewCell.h"
#import "ActionsViewController.h"
#import "MemberLoginViewController.h"
#import "BlurEffectUtils.h"
#import "ScopeUtils.h"
#import "ActionTableViewCellProtocol.h"
#import "AppDataStore.h"
#import "MemberProfileViewController.h"
#import "Social/Social.h"

@interface ActionsTableViewController () <UISearchBarDelegate, UISearchResultsUpdating, ActionTableViewCellProtocol>

@property (nonatomic, strong) NSMutableArray *cachedActionsMutableArray;
@property (nonatomic, strong) NSMutableArray *tableViewActionsMutableArray;
@property (nonatomic, strong) NSMutableArray *filteredActionsMutableArray;

@property (nonatomic, strong) UISearchController *searchController;

@end

@implementation ActionsTableViewController

#
# pragma mark - Lifecycle methods
#
- (void)viewDidLoad {
    [super viewDidLoad];
    //Change login button to profile button
    //if ([AppDataStore sharedManager].currentMember) {
    //    self.userActionButton.title = [AppDataStore sharedManager].currentMember.username;
    //}
    //self.tableView.delegate = self;
    //self.tableView.dataSource = self;
}

- (void) viewWillAppear:(BOOL)animated {
    
    if ([AppDataStore sharedManager].currentMember) {
        self.userActionButton.title = [AppDataStore sharedManager].currentMember.username;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Get all action from server, this should be added to parsemanaer class but how to transfer action and tbale view to new function
    PFQuery *query = [Action query];
    self.tableViewActionsMutableArray = [[NSMutableArray alloc] init];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            
            [PFObject pinAllInBackground:objects withName:@"actions"];
            for (Action *o in objects) {
                [self.tableViewActionsMutableArray addObject:o];
                [self.tableView reloadData];
            }
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
    self.cachedActionsMutableArray = self.tableViewActionsMutableArray;
    
    [self initializeSearchController];
    self.navigationController.navigationBar.backgroundColor = [UIColor action4YouPurpleColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#
# pragma mark - Table view data source
#
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tableViewActionsMutableArray.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Action *actionForIndex = [self.tableViewActionsMutableArray objectAtIndex: [indexPath row]];

    ActionsViewController *actionsViewController = [self.storyboard instantiateViewControllerWithIdentifier:ACTIONS_VIEW_CONTROLLER];
    actionsViewController.mode = VIEW_MODE;
    actionsViewController.action = actionForIndex;
    actionsViewController.actionsArray = self.tableViewActionsMutableArray;
    [self.navigationController pushViewController:actionsViewController animated:YES];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Action *actionForIndex = [self.tableViewActionsMutableArray objectAtIndex: [indexPath row]];

    ActionTableViewCell *actionTableViewCell = [tableView dequeueReusableCellWithIdentifier:ACTION_TABLE_VIEW_CELL forIndexPath:indexPath];
    
    //Protocols properties
    actionTableViewCell.delegate = self;
    actionTableViewCell.cellIndex = indexPath.row;

    actionTableViewCell.actionName.text = actionForIndex.name;
    actionTableViewCell.actionAddress.text = actionForIndex.actionLocation;
    
    dispatch_async(dispatch_get_global_queue(	DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            actionTableViewCell.numberOfParticipants.text = [[NSNumber numberWithLong:actionForIndex.participantCount] stringValue];
        });
    });
    
    actionTableViewCell.actionTime.text = [DateUtils createFromToDateLabel:actionForIndex.dateFrom dateTo: actionForIndex.dateTo];
    
    [actionForIndex.actionImageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        if (!error) {
            actionTableViewCell.actionImage.image = [UIImage imageWithData:data];
        }
    }];
    
    return actionTableViewCell;
}

-(void)didClickOnCellAtIndex:(NSInteger)cellIndex withData:(int)tag {
    
    Action *actionForCellIndex = [self.tableViewActionsMutableArray objectAtIndex: cellIndex];
    ActionsMapViewContoller *actionsMapViewContoller;
    //Facebook controller
    
    switch (tag) {
        case 1:
            //Google maps
            actionsMapViewContoller = [self.storyboard instantiateViewControllerWithIdentifier:ACTIONS_MAP_VIEW_CONTROLLER];
            actionsMapViewContoller.action = actionForCellIndex;
            actionsMapViewContoller.mode = MAP_VIEW_MODE;
            actionsMapViewContoller.actionsArray = self.tableViewActionsMutableArray;
            [self.navigationController pushViewController:actionsMapViewContoller animated:YES];
            break;
    
        case 2:
            //Facebook share
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
                NSString *dateLabel = [DateUtils createFromToDateLabel:actionForCellIndex.dateFrom dateTo:actionForCellIndex.dateTo];
                NSString *facebookText = [NSString stringWithFormat:@"%@\nTime: %@\n%@\n\n%@\n%@\n\n%@",
                                          actionForCellIndex.name,
                                          dateLabel,
                                          actionForCellIndex.actionDesc,
                                          [NSString stringWithFormat: @"%@ %@", NSLocalizedString(@"TELEPHONE", ""), actionForCellIndex.telephone],
                                          [NSString stringWithFormat: @"%@ %@", NSLocalizedString(@"EMAIL", ""), actionForCellIndex.email],
                                          NSLocalizedString(@"APP_NAME", "")];
                
                SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                [controller setInitialText: facebookText];
                [actionForCellIndex.actionImageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                    if (!error) {
                        [controller addImage: [UIImage imageWithData:data]];
                    }
                    [self presentViewController:controller animated:YES completion:nil];
                }];
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"SHARE_ERROR_TITLE", "")
                                                                message: NSLocalizedString(@"FACEBOOK_NOT_AVAILABLE", "")
                                                               delegate: self
                                                      cancelButtonTitle: NSLocalizedString(@"OK", "")
                                                      otherButtonTitles: nil];
                [alert show];
            }
            break;
    }
}

#
# pragma mark - Helper methods
#
- (void)initializeSearchController {
    
    self.definesPresentationContext = NO;

    // Create a mutable array to contain products for the search results table.
    self.filteredActionsMutableArray = [NSMutableArray arrayWithCapacity:[self.tableViewActionsMutableArray count]];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:self.action4YouNavigationViewController];
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.placeholder = NSLocalizedString(@"SEARCH_PLACEHOLDER", "");
    self.searchController.searchBar.frame = CGRectMake(self.searchController.searchBar.frame.origin.x, self.searchController.searchBar.frame.origin.y, self.searchController.searchBar.frame.size.width, 44.0);
    self.searchController.searchBar.backgroundImage = [UIImage imageNamed:SEARCH_BAR_BACKGROUND];
    self.searchController.searchBar.tintColor = [UIColor whiteColor];
    self.searchController.dimsBackgroundDuringPresentation = NO;
    
    //Register custom ActionTableViewCell
    UINib *nib = [UINib nibWithNibName:ACTION_TABLE_VIEW_CELL bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:ACTION_TABLE_VIEW_CELL];
    
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    //Initialize search scopes
    NSMutableArray *scopeButtonTitles = [[NSMutableArray alloc] init];
    for (NSString *scopeType in [ScopeUtils scopeTypes]) {
        NSString *displayName = [ScopeUtils displayNameForType:scopeType];
        [scopeButtonTitles addObject:displayName];
    }
    
    //Scopebar
    self.searchController.searchBar.scopeBarBackgroundImage = [UIImage imageNamed:SEARCH_BAR_BACKGROUND];
    self.searchController.searchBar.scopeButtonTitles = scopeButtonTitles;
    [self.searchController.searchBar setScopeBarButtonTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:APP_FONT_BOLD size:SMALL_MIN_TEXT_SIZE], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    self.searchController.searchBar.delegate = self;
}

#
# pragma mark - IBAction methods
#
- (IBAction)addActionNavigationBarButton:(id)sender {
    ActionsViewController *actionsViewController = [self.storyboard instantiateViewControllerWithIdentifier:ACTIONS_VIEW_CONTROLLER];
    actionsViewController.mode = ADD_EDIT_MODE;
    [self.navigationController pushViewController:actionsViewController animated:YES];
}

- (IBAction)profileNavigationBarButton:(id)sender {
    if ([AppDataStore sharedManager].currentMember) {
        //MemberProfileViewController *memberProfileViewController = [self.storyboard instantiateViewControllerWithIdentifier:MEMBER_PROFILE_VIEW_CONTROLLER];
        //[self.navigationController pushViewController:memberProfileViewController animated:YES];
        MemberLoginViewController *memberLoginViewController = [self.storyboard instantiateViewControllerWithIdentifier:MEMBER_LOGIN_VIEW_CONTROLLER];
        [self.navigationController pushViewController:memberLoginViewController animated:YES];
    } else {
        MemberLoginViewController *memberLoginViewController = [self.storyboard instantiateViewControllerWithIdentifier:MEMBER_LOGIN_VIEW_CONTROLLER];
        [self.navigationController pushViewController:memberLoginViewController animated:YES];
    }
}

#
#pragma mark - UISearchResultsUpdating
#
-(void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    NSString *searchString = [self.searchController.searchBar text];
    NSString *scope = nil;
    
    NSInteger selectedScopeButtonIndex = [self.searchController.searchBar selectedScopeButtonIndex];
    if (selectedScopeButtonIndex > 0) {
        scope = [[ScopeUtils scopeTypes] objectAtIndex:(selectedScopeButtonIndex)];
    }
    
    [self updateFilteredContentForProductName:searchString type:scope];
    
    if(self.filteredActionsMutableArray.count > 0) {
        self.tableViewActionsMutableArray = self.filteredActionsMutableArray;
    } else {
        self.tableViewActionsMutableArray = self.cachedActionsMutableArray;
    }
    [self.tableView reloadData];
    
}

#
#pragma mark - UISearchBarDelegate
#
// Workaround for bug: -updateSearchResultsForSearchController: is not called when scope buttons change
- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope {
    self.tableViewActionsMutableArray = self.cachedActionsMutableArray;
    [self updateSearchResultsForSearchController:self.searchController];
}

#
#pragma mark - Search Content Filtering
#
- (void)updateFilteredContentForProductName:(NSString *)actionName type:(NSString *)typeName {
    
    // Update the filtered array based on the search text and scope.
    if ((actionName == nil) || [actionName length] == 0) {
        // If there is no search string and the scope is "All".
        if (typeName == nil) {
            self.filteredActionsMutableArray = [self.tableViewActionsMutableArray mutableCopy];
        } else {
            // If there is no search string and the scope is chosen.
            NSMutableArray *searchResults = [[NSMutableArray alloc] init];
            for (Action *action in self.tableViewActionsMutableArray) {
                if ([action.type isEqualToString:typeName]) {
                    [searchResults addObject:action];
                }
            }
            self.filteredActionsMutableArray = searchResults;
        }
        return;
    }
    
    
    [self.filteredActionsMutableArray removeAllObjects]; // First clear the filtered array.
    
    //Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
    for (Action *action in self.tableViewActionsMutableArray) {
        if ((typeName == nil) || [action.name isEqualToString:typeName]) {
            NSUInteger searchOptions = NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch;
            NSRange productNameRange = NSMakeRange(0, action.name.length);
            NSRange foundRange = [action.name rangeOfString:actionName options:searchOptions range:productNameRange];
            if (foundRange.length > 0) {
                [self.filteredActionsMutableArray addObject:action];
            }
        }
    }
}

@end
