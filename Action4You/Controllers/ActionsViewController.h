//
//  AddEditActionsViewController.h
//  Action4You
//
//  Created by Dominik Koscica on 13/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ActionsViewController : BaseViewController

@property (nonatomic) ActionsViewControllerMode mode;
@property (nonatomic) Action *action;
@property (nonatomic, strong) NSMutableArray *actionsArray;

@property (nonatomic, strong) IBOutlet Action4YouTextField *name;
@property (nonatomic, strong) IBOutlet Action4YouImageView *controllerBackground;
@property (nonatomic, strong) IBOutlet Action4YouImageView *actionImageView;
@property (nonatomic, strong) IBOutlet Action4YouTextView *information;
@property (nonatomic, strong) IBOutlet Action4YouLabel *startdateAndTime;
@property (nonatomic, strong) IBOutlet Action4YouLabel *endDateAndTime;
@property (nonatomic, strong) IBOutlet Action4YouLabel *address;
@property (nonatomic, strong) IBOutlet Action4YouTextField *telephone;
@property (nonatomic, strong) IBOutlet Action4YouTextField *email;
@property (nonatomic, strong) IBOutlet UITabBarItem *takeAction;
@property (nonatomic, strong) IBOutlet UITabBarItem *participantCountItem;

@end
