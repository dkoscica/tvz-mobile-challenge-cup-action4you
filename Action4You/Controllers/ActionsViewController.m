//
//  AddEditActionsViewController.m
//  Action4You
//
//  Created by Dominik Koscica on 13/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "ActionsViewController.h"
#import "MBProgressHUD.h"
#import <ActionSheetPicker-3.0/ActionSheetDatePicker.h>
#import "ActionsMapViewContoller.h"
#import "MemberLoginViewController.h"
#import "SendActionProtocol.h"

@interface ActionsViewController () <UITabBarDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, UITextFieldDelegate, SendActionProtocol>

@property (nonatomic, strong) IBOutlet UITabBar *viewTabBar;
@property (nonatomic, strong) IBOutlet UITabBar *addEditTabBar;
@property (nonatomic, strong) UITapGestureRecognizer *viewTapGesture;
@property (nonatomic, weak) UIImage *imageFromImagePicker;

@end

@implementation ActionsViewController

#
# pragma mark - Lifecycle methods
#
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeAction];
    [self setupDelegates];
}

- (void) viewWillAppear:(BOOL)animated {
    [self setupController];
    [self setupNavigationBar:self.action.actionTypeInt];
    [self setupTabBar];
    [self setupTapGestures];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#
# pragma mark - Delegate methods
#
- (void)setupDelegates {
    self.information.delegate = self;
    self.telephone.delegate = self;
    self.email.delegate = self;
}

- (void)sendActionLocation:(NSMutableString *)actionLocation : (double)latitude : (double)longitude {
    if(self.mode == ADD_EDIT_MODE) {
        self.address.text = actionLocation;
        self.action.locationLatitude = latitude;
        self.action.locationLongitude = longitude;
        [self setupNavigationBar:self.action.actionTypeInt];
    }
}

#
# pragma mark - UITextField ScrollView - Keyboard
#
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self animateTextField: textField up: YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self animateTextField: textField up: NO];
}

- (void)animateTextField: (UITextField*) textField up: (BOOL) up {
    const int movementDistance = 160; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

#
# pragma mark - UITextView
#
-(void)textViewDidBeginEditing:(UITextView *)textView{
    self.information.text = nil;
    self.viewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap:)];
    [self.view addGestureRecognizer:self.viewTapGesture];
}

#
# pragma mark - IBAction methods
#
- (void)tabBar:(UITabBar *)uiTabBar didSelectItem:(UITabBarItem *)item {
    
    
    if(self.viewTabBar == uiTabBar) {
    
        switch(item.tag) {
                
            case 1:
                //Participans count
                break;
                
            case 2:
                [self pushToActionsMapViewController:MAP_VIEW_MODE];
                break;
                
            case 3:
                //Prijava na akciju
                [self takeActionTap:item];
                break;
        }
    
    } else if(self.addEditTabBar == uiTabBar) {
     
            switch(item.tag) {
                
                case 1:
                    self.action.type = HUMANITARIAN;
                    [self setupNavigationBar:1];
                    break;
                
                case 2:
                    self.action.type = EDUCATION;
                    [self setupNavigationBar:2];
                    break;
                
                case 3:
                    self.action.type = WORK;
                    [self setupNavigationBar:3];
                    break;
                
                case 4:
                    self.action.type = SHARE;
                    [self setupNavigationBar:4];
                    break;
            }
    }
}

- (IBAction)navigationBarSaveButton:(id)sender {
    
    if([self validateForm] == TRUE) {
    
        // Convert to JPEG with 25% quality
        NSData* data = UIImageJPEGRepresentation(self.actionImageView.image, 0.25f);
        PFFile *imageFile = [PFFile fileWithName:IMAGE_FILE_NAME data:data];
    
        // Save the image to Parse
        [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                // The image has now been uploaded to Parse. Associate it with a new object
                Action *actions = [[Action new] initActionWithName:[self.name.text mutableCopy]
                           description:[self.information.text mutableCopy]
                       actionImageFile:imageFile
                              dateFrom:self.action.dateFrom
                                dateTo:self.action.dateTo
                        actionLocation:[self.address.text mutableCopy]
                     locactionLatitude:self.action.locationLatitude
                     locationLongitude:self.action.locationLongitude
                            actionType:self.action.type
                      participantCount:0
                               country:[self.address.text mutableCopy]
                                  town:[self.address.text mutableCopy]
                             organizer: [AppDataStore sharedManager].currentMember
                            telephone:[self.telephone.text mutableCopy]
                                email:[self.email.text mutableCopy]];
                [actions setObject:imageFile forKey:ACTION_IMAGE_FILE_KEY];
            
                [actions saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    if (!error) {
                    NSLog(@"Saved");
                    } else {
                        // Error
                        NSLog(@"Error: %@ %@", error, [error userInfo]);
                    }
                    [self.navigationController popViewControllerAnimated:TRUE];
                }];
            }
        }];
    }
}

- (void)dateAndTimeTap:(UITapGestureRecognizer *)gestureRecognizer  {
    
    Action4YouLabel *label = (Action4YouLabel *)gestureRecognizer.view;
    
    [ActionSheetDatePicker showPickerWithTitle:NSLocalizedString(@"DATE_PICKER_TITLE", "")
                                datePickerMode:UIDatePickerModeDateAndTime
                                  selectedDate:[NSDate date]
                                     doneBlock:^(ActionSheetDatePicker *picker, NSDate *selectedDate, id origin) {
                                         if(label.tag == 1) {
                                             self.action.dateFrom = selectedDate;
                                             self.startdateAndTime.text = [DateUtils stringFromDate:selectedDate];
                                             return;
                                         }
                                         self.action.dateTo = selectedDate;
                                         self.endDateAndTime.text = [DateUtils stringFromDate:selectedDate];
                                         
                                     } cancelBlock:nil origin:label];
}

- (void)locationTap:(id)sender  {
    [self pushToActionsMapViewController:MAP_ADD_EDIT_MODE];
}

- (void)imageTap:(UITapGestureRecognizer *)gestureRecognizer  {
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    imagePicker.allowsEditing = YES;
    
    
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:YES completion:NULL];
    
}

- (void)viewTap:(id)sender  {
    //Used for dismissing the keyboard
    [[self view] endEditing: YES];
    
    //UITextView and telephone
    [self.view removeGestureRecognizer:self.viewTapGesture];
}

- (IBAction)telephone:(id)sender {
    self.viewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap:)];
    [self.view addGestureRecognizer:self.viewTapGesture];
}

#
# pragma mark - IBAction methods - VIEW_MODE
#

- (void)takeActionTap: (id)sender {
    NSLog(@"Take action tap");
    
    Member *member = [AppDataStore sharedManager].currentMember;
    if (member) {
        MBProgressHUD *progress = [MBProgressHUD showHUDAddedTo: self.view animated:YES];
        progress.mode = MBProgressHUDModeText;
        progress.labelText = NSLocalizedString(@"YOU_ARE_A_PARTICIPANT", "");
        [progress hide:YES afterDelay: 2];
        [self.action addActionPaticipant:member];
        [self.action saveInBackground];
        [member addActionForMember:self.action];
        [member saveInBackground];
        [self.takeAction setTitle:NSLocalizedString(@"SUBSCRIBED", "")];
        [self.takeAction setEnabled:NO];
        self.participantCountItem.title = [NSString stringWithFormat:@"%lu", self.action.participantCount];
    } else {
        MemberLoginViewController *memberLoginViewController = [self.storyboard instantiateViewControllerWithIdentifier:MEMBER_LOGIN_VIEW_CONTROLLER];
        [self.navigationController pushViewController:memberLoginViewController animated:YES];
    }
}




#
#pragma mark - Camera
#
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    self.imageFromImagePicker = info[UIImagePickerControllerOriginalImage];
    self.actionImageView.image = self.imageFromImagePicker;
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#
# pragma mark - Helper methods
#
- (void)pushToActionsMapViewController: (int)mapMode {
    ActionsMapViewContoller *actionsMapViewContoller = [self.storyboard instantiateViewControllerWithIdentifier:ACTIONS_MAP_VIEW_CONTROLLER];
    actionsMapViewContoller.delegate=self; // protocol listener
    actionsMapViewContoller.mode = mapMode;
    actionsMapViewContoller.action = self.action;
    actionsMapViewContoller.actionsArray = self.actionsArray;
    [self.navigationController pushViewController:actionsMapViewContoller animated:YES];
}

- (void)setupNavigationBar:(int)actionType {
    
    switch (actionType) {
        case 1:
            self.title = NSLocalizedString(@"HUMANITARIAN", "");
            self.controllerBackground.image = [UIImage imageNamed:HUMANITARIAN_BLUR_BACKGROUND];
            //self.actionImageView.image = [UIImage imageNamed:HUMANITARIAN_LANDSCAPE_BACKGROUND];
            [self.action4YouNavigationViewController changeNavigationBarColor:[UIColor action4YouGreenColor]];
            [self checkIfActionImageViewNeedToBeReplaced:HUMANITARIAN_LANDSCAPE_BACKGROUND];
            break;
            
        case 2:
            self.title = NSLocalizedString(@"EDUCATION", "");
            self.controllerBackground.image = [UIImage imageNamed:EDUCATION_BLUR_BACKGROUND];
            //self.actionImageView.image = [UIImage imageNamed:EDUCATION_LANDSCAPE_BACKGROUND];
            [self.action4YouNavigationViewController changeNavigationBarColor:[UIColor action4YouBlueColor]];
            [self checkIfActionImageViewNeedToBeReplaced:EDUCATION_LANDSCAPE_BACKGROUND];
            break;
            
        case 3:
            self.title = NSLocalizedString(@"WORK", "");
            self.controllerBackground.image = [UIImage imageNamed:WORK_BLUR_BACKGROUND];
            //self.actionImageView.image = [UIImage imageNamed:WORK_LANDSCAPE_BACKGROUND];
            [self.action4YouNavigationViewController changeNavigationBarColor:[UIColor action4YouRedColor]];
            [self checkIfActionImageViewNeedToBeReplaced:WORK_LANDSCAPE_BACKGROUND];
            break;
            
        case 4:
            self.title = NSLocalizedString(@"SHARE", "");
            self.controllerBackground.image = [UIImage imageNamed:SHARE_BLUR_BACKGROUND];
            //self.actionImageView.image = [UIImage imageNamed:SHARE_LANDSCAPE_BACKGROUND];
            [self.action4YouNavigationViewController changeNavigationBarColor:[UIColor action4YouYellowColor]];
            [self checkIfActionImageViewNeedToBeReplaced:SHARE_LANDSCAPE_BACKGROUND];
            break;
            
        default:
            self.title = NSLocalizedString(@"HUMANITARIAN", "");
            self.controllerBackground.image = [UIImage imageNamed:HUMANITARIAN_BLUR_BACKGROUND];
            //self.actionImageView.image = [UIImage imageNamed:HUMANITARIAN_LANDSCAPE_BACKGROUND];
            [self.action4YouNavigationViewController changeNavigationBarColor:[UIColor action4YouGreenColor]];
            [self checkIfActionImageViewNeedToBeReplaced:HUMANITARIAN_LANDSCAPE_BACKGROUND];
            break;
    }
}

- (void)setupTabBar {
    
    [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.0]];
    
    //Unselected - default
    //[[UITabBarItem appearance] setImage: [[[UIImage imageNamed:@"action4you_people_icon.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    //[[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]} forState:UIControlStateNormal];
    
    //Selected
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]} forState:UIControlStateSelected];
    //Set participant count
    if (self.mode == VIEW_MODE) {
        dispatch_async(dispatch_get_global_queue(	DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            long count = [self.action participantCount];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.participantCountItem setTitle: [NSString stringWithFormat:@"%lu", count]];
            });
            if ([ParseStore isCurrentMemberParticipantInAction:self.action]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.takeAction setTitle:@"Subscribed"];
                    [self.takeAction setEnabled:NO];
                });
            }
            
        });
    }
}

- (void)setupTapGestures {
    UITapGestureRecognizer *starDateAndTimeTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dateAndTimeTap:)];
    UITapGestureRecognizer *endDateAndTimeTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dateAndTimeTap:)];
    UITapGestureRecognizer *locationTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(locationTap:)];
    UITapGestureRecognizer *imageTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTap:)];
    
    [self.startdateAndTime addGestureRecognizer:starDateAndTimeTapGesture];
    [self.endDateAndTime addGestureRecognizer:endDateAndTimeTapGesture];
    [self.address addGestureRecognizer:imageTapGesture];
    [self.actionImageView addGestureRecognizer:imageTapGesture];
    [self.address addGestureRecognizer:locationTapGesture];
}

- (void) initializeAction {

    Action *action = self.action;
    
    if(action) {
        self.name.text = action.name;
        self.information.text = action.actionDesc;
        self.startdateAndTime.text = [DateUtils stringFromDate:action.dateFrom];
        self.endDateAndTime.text = [DateUtils stringFromDate:action.dateTo];
        self.address.text = action.actionLocation;
        self.telephone.text = action.telephone;
        self.email.text = action.email;
        
        //Update actionImageView with the image from parse
        [self.action.actionImageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            if (!error) {
                self.actionImageView.image = [UIImage imageWithData:data];
            }
        }];
        
    } else {
        self.action = [Action new];
    }
}

- (void)setupController {
    if(self.mode == VIEW_MODE){
        self.viewTabBar.hidden = NO;
        self.addEditTabBar.hidden = YES;
        self.navigationItem.rightBarButtonItem = nil;
        [self disableUIElementsForViewMode];
        
    } else if(self.mode == ADD_EDIT_MODE){
        self.viewTabBar.hidden = YES;
        self.addEditTabBar.hidden = NO;
    }
}

- (void)checkIfActionImageViewNeedToBeReplaced:(NSString*)imageName {
    if(!self.imageFromImagePicker) {
        self.actionImageView.image = [UIImage imageNamed:imageName];
    }
}

- (void)disableUIElementsForViewMode {
    
    self.name.enabled = NO;
    self.information.editable = NO;
    self.startdateAndTime.userInteractionEnabled = NO;
    self.endDateAndTime.userInteractionEnabled = NO;
    self.address.userInteractionEnabled = NO;
    self.telephone.enabled = NO;
    self.email.enabled = NO;

}

- (BOOL)validateForm {
    
    [ValidatorUtils invalidateView:self.name];
    [ValidatorUtils invalidateView:self.information];
    [ValidatorUtils invalidateView:self.startdateAndTime];
    [ValidatorUtils invalidateView:self.endDateAndTime];
    [ValidatorUtils invalidateView:self.telephone];
    [ValidatorUtils invalidateView:self.email];

    if(![self.name.text isValidName]) {
        [ValidatorUtils changeUITextFieldToInvalid:self.name : NSLocalizedString(@"NAME_ERROR", "")];
        return FALSE;
        
    } else if(![self.information.text isValidInformation]) {
        [ValidatorUtils changeUITextViewToInvalid:self.information : NSLocalizedString(@"INFORMATION_ERROR", "")];
        return FALSE;
        
    } else if(![self.startdateAndTime.text isValidStartDateAndTime]) {
        [ValidatorUtils changeUILabelToInvalid:self.startdateAndTime : NSLocalizedString(@"START_DATE_ERROR", "")];
        return FALSE;
        
    } else if(![self.endDateAndTime.text isValidEndDateAndTime]) {
        [ValidatorUtils changeUILabelToInvalid:self.endDateAndTime : NSLocalizedString(@"END_DATE_ERROR", "")];
        return FALSE;
        
    } else if(![self.address.text isValidAddress]) {
        [ValidatorUtils changeUILabelToInvalid:self.address : NSLocalizedString(@"ADDRESS_ERROR", "")];
        return FALSE;
        
    } else if(![self.telephone.text isValidTelephone]) {
        [ValidatorUtils changeUITextFieldToInvalid:self.telephone : NSLocalizedString(@"TELEPHONE_ERROR", "")];
        return FALSE;
        
    } else if(![self.email.text isValidEmail]) {
        [ValidatorUtils changeUITextFieldToInvalid:self.email : NSLocalizedString(@"EMAIL_ERROR", "")];
        return FALSE;
    }
    
    return TRUE;
}

//Dismiss keyboard on done
-(IBAction)editingEnded:(id)sender{
    [sender resignFirstResponder];
}

@end
