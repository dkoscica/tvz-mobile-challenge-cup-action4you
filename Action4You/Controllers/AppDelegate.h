//
//  AppDelegate.h
//  TVZ-Mc2-App
//
//  Created by Dominik Koscica on 14/03/15.
//  Copyright (c) 2015 Dominik Koscica. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

