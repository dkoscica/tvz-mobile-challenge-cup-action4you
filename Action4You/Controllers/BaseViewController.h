//
//  BaseViewController.h
//  TVZ-Mc2-App
//
//  Created by Dominik Koscica on 06/04/15.
//  Copyright (c) 2015 Dominik Koscica. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "Constants.h"
#import "Action.h"
#import "Member.h"
#import "ActionTypes.h"
#import "MemberContact.h"
#import "DateUtils.h"
#import "Action4YouTextField.h"
#import "Action4YouTextView.h"
#import "Action4YouImageView.h"
#import "Action4YouLabel.h"
#import "UIColor+Action4YouColors.h"
#import "ActionsViewControllerMode.h"
#import "ActionsMapViewControllerMode.h"
#import "Action4YouNavigationViewController.h"
#import "AppDataStore.h"
#import "ParseStore.h"
#import "NSString+FormValidation.h"
#import "ValidatorUtils.h"
#import "ActionsTableViewController.h"
#import "MBProgressHUD.h"

@interface BaseViewController : UIViewController 

@property(nonatomic, readonly) Action4YouNavigationViewController *action4YouNavigationViewController;

- (void)pushControllerName: (NSString *) controllerName;

@end
