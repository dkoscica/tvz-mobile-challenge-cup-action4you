//
//  BaseViewController.m
//  TVZ-Mc2-App
//
//  Created by Dominik Koscica on 06/04/15.
//  Copyright (c) 2015 Dominik Koscica. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

#
# pragma mark - Lifecycle methods
#
- (void)viewDidLoad {
    [super viewDidLoad];    
    //[self addBlurEffect];
}

- (Action4YouNavigationViewController*)action4YouNavigationViewController {
    return (Action4YouNavigationViewController*)self.navigationController;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#
# pragma mark - Helper methods
#
- (void)pushControllerName:(NSString *)controllerName {
    BaseViewController *action4YouNavigationViewController = [self.storyboard instantiateViewControllerWithIdentifier:controllerName];
    [self presentViewController:action4YouNavigationViewController animated:NO completion:nil];
}

- (void) addBlurEffect {
    // Add blur view
    CGRect bounds = self.navigationController.navigationBar.bounds;
    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
    visualEffectView.frame = bounds;
    visualEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.navigationController.navigationBar addSubview:visualEffectView];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar sendSubviewToBack:visualEffectView];
    
    // Here you can add visual effects to any UIView control.
    // Replace custom view with navigation bar in above code to add effects to custom view.
}

//Dismiss keyboard on done
-(IBAction)editingEnded:(id)sender{
    [sender resignFirstResponder];
}

@end
