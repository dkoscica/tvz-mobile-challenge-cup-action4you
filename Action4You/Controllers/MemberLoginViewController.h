//
//  MemberLoginViewController.h
//  Action4You
//
//  Created by Bruno Grizli on 4/20/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface MemberLoginViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UITextField *username;
@property (nonatomic, weak) IBOutlet UITextField *password;
@property (nonatomic, weak) IBOutlet UIButton *loginButton;
- (IBAction)loginUser:(id)sender;

@end
