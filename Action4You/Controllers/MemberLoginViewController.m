//
//  MemberLoginViewController.m
//  Action4You
//
//  Created by Bruno Grizli on 4/20/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "MemberLoginViewController.h"

@interface MemberLoginViewController ()

@end

@implementation MemberLoginViewController

#
# pragma mark - Lifecycle methods
#
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#
# pragma mark - IBAction methods
#
- (IBAction)loginUser:(id)sender {
    
    if([self validateForm] == TRUE) {
    
        MBProgressHUD *progress = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        progress.labelText = NSLocalizedString(@"PROGRESS_TITLE", "");
        dispatch_async(dispatch_get_main_queue(), ^{
        //Login user
        PFUser *user = [PFUser new];
        user.password = self.password.text;
        user.username = self.username.text;
        if([PFUser logInWithUsername:user.username password:user.password]) {
            [ParseStore pinCurrentUser:user];
            ActionsTableViewController *actionsTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:ACTIONS_TABLE_VIEW_CONTROLLER];
            actionsTableViewController.userActionButton.title = user.username;
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"LOGIN_ERROR", "")
                                                            message: NSLocalizedString(@"WRONG_USERNAME_OR_PASSWORD", "")
                                                           delegate: self
                                                  cancelButtonTitle: NSLocalizedString(@"OK", "")
                                                   otherButtonTitles: nil];
            [alert show];
            
        }
            
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        });
    }
}

- (BOOL)validateForm {
    
    [ValidatorUtils invalidateView:self.username];
    [ValidatorUtils invalidateView:self.password];
    
    if(![self.username.text isValidUsername]) {
        [ValidatorUtils changeUITextFieldToInvalid:self.username : @"USERNAME_ERROR"];
        return FALSE;
        
    } else if(![self.password.text isValidPassword]) {
        [ValidatorUtils changeUITextFieldToInvalid:self.password : @"PASSWORD_ERROR"];
        return FALSE;
    }
    return TRUE;
}

//Dismiss keyboard on done
-(IBAction)editingEnded:(id)sender{
    [sender resignFirstResponder];
}

@end
