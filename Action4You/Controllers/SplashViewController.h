//
//  SplashViewController.h
//  TVZ-Mc2-App
//
//  Created by Dominik Koscica on 15/03/15.
//  Copyright (c) 2015 Dominik Koscica. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface SplashViewController : BaseViewController

@end
