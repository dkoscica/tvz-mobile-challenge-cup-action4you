//
//  SplashViewController.m
//  TVZ-Mc2-App
//
//  Created by Dominik Koscica on 15/03/15.
//  Copyright (c) 2015 Dominik Koscica. All rights reserved.
//

#import "SplashViewController.h"

@interface SplashViewController ()

@end

//iOS Lifecycle
//http://3.bp.blogspot.com/-QVJKpBN-a_Y/T2DPT6HPuiI/AAAAAAAAAp0/neDL4hb51gA/s1600/Learn+iOS+LifeCycle+from+Android+LifeCycle.png

@implementation SplashViewController

#
# pragma mark - Lifecycle methods
#
- (void) viewDidLoad {
    sleep(1);
    PFQuery *currentUserQuery = [Member query];
    [currentUserQuery fromLocalDatastore];
    NSArray *users = [currentUserQuery findObjects];
    Member *memberFromDb = [users firstObject];
    if (memberFromDb) {
        [AppDataStore sharedManager].currentMember = memberFromDb;
    }
    
}

- (void) viewWillAppear:(BOOL)animated {
}


- (void) viewDidAppear:(BOOL)animated {
    //IMPORTANT pushControllerName must be called in viewDidAppear!!!
    [super pushControllerName:NAVIGATION_VIEW_CONTROLLER];
}
     


@end