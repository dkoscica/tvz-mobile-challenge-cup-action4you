//
//  ActionsMapViewControllerMode.h
//  Action4You
//
//  Created by Dominik Košćica on 28/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

typedef enum{
    MAP_ADD_EDIT_MODE,
    MAP_VIEW_MODE
} ActionsMapViewControllerMode;
