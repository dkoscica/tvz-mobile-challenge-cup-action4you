//
//  Header.h
//  Action4You
//
//  Created by Dominik Koscica on 19/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

typedef enum{
    ADD_EDIT_MODE = 1,
    VIEW_MODE,
} ActionsViewControllerMode;