//
//  BlurEffectUtils.h
//  Action4You
//
//  Created by Dominik Koscica on 22/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BlurEffectUtils : NSObject

+ (void) addBlurEffect:(UIView *)view;

@end
