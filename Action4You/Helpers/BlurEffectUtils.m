//
//  BlurEffectUtils.m
//  Action4You
//
//  Created by Dominik Koscica on 22/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "BlurEffectUtils.h"

@implementation BlurEffectUtils

//Not yet tested!!!!
+ (void) addBlurEffect:(UIView *)view {
    // Add blur view
    CGRect bounds = view.bounds;
    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
    visualEffectView.frame = bounds;
    visualEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [view addSubview:visualEffectView];
    [view setBackgroundColor: [UIColor clearColor]];
    //[self.sendSubviewToBack:visualEffectView];
    
    // Here you can add visual effects to any UIView control.
    // Replace custom view with navigation bar in above code to add effects to custom view.
}

@end
