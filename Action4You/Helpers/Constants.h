//
//  Constants.h
//  TVZ-Mc2-App
//
//  Created by Dominik Koscica on 14/03/15.
//  Copyright (c) 2015 Dominik Koscica. All rights reserved.
//

#import <Foundation/Foundation.h>

//API Keys
static NSString *const GOOGLE_MAPS_KEY = @"AIzaSyCTnFCCjH7gXF7ZJhp-x08gi3NTadYY84g";
static NSString *const PARSE_APP_ID = @"ji86s86xf151sDgabwtThXsPPDfUiMF2iSj36fL4";
static NSString *const PARSE_CLIENT_KEY = @"WrD5INat6gjRivEmNfN8M0vTV9DeIDcJWLkgOUQq";

//Parse
static NSString *const IMAGE_FILE_NAME = @"Image.jpg";
static NSString *const ACTION_IMAGE_FILE_KEY = @"actionImageFile";

//Navigation Controller Constants
static NSString *const SPLASH_VIEW_CONTROLLER = @"SplashViewController";
static NSString *const BASE_VIEW_CONTROLLER = @"BaseViewController";
static NSString *const NAVIGATION_VIEW_CONTROLLER = @"NavigationViewController";
static NSString *const ACTIONS_TABLE_VIEW_CONTROLLER = @"ActionsTableViewController";
static NSString *const ACTIONS_MAP_VIEW_CONTROLLER = @"ActionsMapViewController";
static NSString *const ABOUT_VIEW_CONTROLLER = @"AboutViewController";
static NSString *const ACTIONS_VIEW_CONTROLLER = @"ActionsViewController";
static NSString *const MEMBER_LOGIN_VIEW_CONTROLLER = @"MemberLoginViewController";
static NSString *const MEMBER_PROFILE_VIEW_CONTROLLER = @"MemberProfileViewController";

//UI Constants
static NSString *const ACTION_TABLE_VIEW_CELL = @"ActionTableViewCell";
static int const CORNER_RADIUS = 2;
static int const LEFT_MARGIN = 6.0;
static int const VIEW_HEIGHT = 35;
static double const BORDER_WIDTH = 0.6;

//Assets Constatns
static NSString *const SEARCH_BAR_BACKGROUND = @"search_bar_background@2x.png";
static NSString *const HUMANITARIAN_BLUR_BACKGROUND = @"humanitarian_blur@2x.png";
static NSString *const EDUCATION_BLUR_BACKGROUND = @"education_blur@2x.png";
static NSString *const WORK_BLUR_BACKGROUND = @"work_blur@2x.png";
static NSString *const SHARE_BLUR_BACKGROUND = @"share_donate_blur@2x.png";

static NSString *const HUMANITARIAN_LANDSCAPE_BACKGROUND = @"humanitarian_landscape@2x.png";
static NSString *const EDUCATION_LANDSCAPE_BACKGROUND = @"education_landscape@2x.png";
static NSString *const WORK_LANDSCAPE_BACKGROUND = @"share_donate_blur@2x.png";
static NSString *const SHARE_LANDSCAPE_BACKGROUND = @"share_donate_landscape@2x.png";

static NSString *const MAP_MARKER_HUMANITARIAN = @"map_marker_humanitarian@2x.png";
static NSString *const MAP_MARKER_EDUCATION = @"map_marker_education@2x.png";;
static NSString *const MAP_MARKER_WORK = @"map_marker_work@2x.png";;
static NSString *const MAP_MARKER_SHARE = @"map_marker_share@2x.png";;

static NSString *const ERROR_ICON = @"error_icon";


//Font Constans
static NSString *const APP_FONT = @"AppleSDGothicNeo-Regular";
static NSString *const APP_FONT_BOLD = @"AppleSDGothicNeo-Bold";
static NSString *const APP_FONT_LIGHT = @"AppleSDGothicNeo-Light";

static double const SMALL_TEXT_SIZE = 12.0f;
static double const SMALL_MIN_TEXT_SIZE = 9.0f;

static double const NORMAL_TEXT_SIZE = 16.0f;
static double const NORMAL_MIN_TEXT_SIZE = 14.0f;

static double const LARGE_TEXT_SIZE = 18.0f;
static double const LARGE_MIN_TEXT_SIZE = 16.0f;

//Google Maps Constants
static double const ZOOM_LEVEL = 12;


