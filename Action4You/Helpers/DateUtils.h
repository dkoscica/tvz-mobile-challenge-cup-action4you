//
//  DateUtils.h
//  Action4You
//
//  Created by Bruno Grizli on 4/13/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateUtils : NSObject

+ (NSString *) createFromToDateLabel: (NSDate *)dateFrom dateTo: (NSDate *)dateTo;
+ (NSString *) stringFromDate: (NSDate *)date;

@end
