//
//  DateUtils.m
//  Action4You
//
//  Created by Bruno Grizli on 4/13/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "DateUtils.h"

@implementation DateUtils

+ (NSString *)createFromToDateLabel: (NSDate *)dateFrom dateTo: (NSDate *)dateTo {
    NSString *dateStr = [NSString new];
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat: @"dd.MM.yy"];
    dateStr = [NSString stringWithFormat: @"%@ - %@", [formatter stringFromDate: dateFrom], [formatter stringFromDate: dateTo]];
    return dateStr;
}

+ (NSString *)stringFromDate:(NSDate *)date {
    NSDateFormatter* dateFormater = [NSDateFormatter new];
    [dateFormater setDateFormat:@"dd/MM/yyyy HH:mm"];
    return [dateFormater stringFromDate:date];
}

@end

