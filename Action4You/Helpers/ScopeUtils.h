//
//  ScopeUtils.h
//  Action4You
//
//  Created by Dominik Koscica on 25/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ScopeUtils : NSObject

+ (NSArray *)scopeTypes;
+ (NSString *)displayNameForType:(NSString *)type;

@end
