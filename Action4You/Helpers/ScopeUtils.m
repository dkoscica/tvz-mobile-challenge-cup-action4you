//
//  ScopeUtils.m
//  Action4You
//
//  Created by Dominik Koscica on 25/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "ScopeUtils.h"


@implementation ScopeUtils

static NSString *const ALL = @"All";
static NSString *const HUMANITARIAN = @"Humanitarian";
static NSString *const EDUCATION = @"Education";
static NSString *const WORK = @"Work";
static NSString *const SHARE = @"Share";

+ (NSArray *)scopeTypes {
    static NSArray *scopeTypes = nil;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        scopeTypes = @[ALL, HUMANITARIAN, EDUCATION, WORK, SHARE];
    });
    
    return scopeTypes;
}

+ (NSString *)displayNameForType:(NSString *)type {
    static NSMutableDictionary *scopeTypeDisplayNamesDictionary = nil;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        scopeTypeDisplayNamesDictionary = [[NSMutableDictionary alloc] init];
        for (NSString *actionType in self.scopeTypes) {
            NSString *displayName = NSLocalizedString(actionType, @"dynamic");
            scopeTypeDisplayNamesDictionary[actionType] = displayName;
        }
    });
    
    return scopeTypeDisplayNamesDictionary[type];
}

@end
