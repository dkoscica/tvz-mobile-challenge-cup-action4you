//
//  ValidatorUtils.h
//  Action4You
//
//  Created by Dominik Koscica on 27/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import <Foundation/Foundation.h>
#import  <UIKit/UIKit.h>

@interface ValidatorUtils : NSObject

+ (void)changeUITextViewToInvalid:(UITextView *)uiTextView :(NSString*)localizedString;
+ (void)changeUITextFieldToInvalid:(UITextField *)uiTextField :(NSString*)localizedString;
+ (void)changeUILabelToInvalid:(UILabel *)uiLabel :(NSString*)localizedString;
+ (void)invalidateView:(UIView *)view;

@end
