//
//  ValidatorUtils.m
//  Action4You
//
//  Created by Dominik Koscica on 27/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "ValidatorUtils.h"
#import "Constants.h"

@implementation ValidatorUtils

+ (void)changeUITextViewToInvalid:(UITextView *)uiTextView :(NSString*)localizedString{
    if (uiTextView){
        uiTextView.text = NSLocalizedString(localizedString, "");
        uiTextView.layer.borderColor = [UIColor redColor].CGColor;
    }
}

+ (void)changeUITextFieldToInvalid:(UITextField *)uiTextField :(NSString*)localizedString{
    if (uiTextField){
        uiTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(localizedString, "") attributes:@{NSForegroundColorAttributeName: [UIColor redColor]}];
        uiTextField.text = nil;
        uiTextField.layer.borderColor = [UIColor redColor].CGColor;
    }
}

+ (void)changeUILabelToInvalid:(UILabel *)uiLabel :(NSString*)localizedString{
    if (uiLabel){
        uiLabel.text = NSLocalizedString(localizedString, "") ;
        uiLabel.layer.borderColor = [UIColor redColor].CGColor;
    }
}

+ (void)invalidateView:(UIView *)view {
    view.layer.borderColor = [UIColor whiteColor].CGColor;
}

@end
