//
//  Action.h
//  Action4You
//
//  Created by Bruno Grizli on 4/11/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import "Parse/Parse.h"
@class Member;

static NSString *const HUMANITARIAN = @"Humanitarian";
static NSString *const EDUCATION = @"Education";
static NSString *const WORK = @"Work";
static NSString *const SHARE = @"Share";

@interface Action : PFObject<PFSubclassing>

@property(nonatomic, strong) NSMutableString *name;
@property(nonatomic, strong) NSMutableString *actionDesc;
@property(nonatomic, strong) PFFile *actionImageFile;
@property(nonatomic, strong) NSDate *dateFrom;
@property(nonatomic, strong) NSDate *dateTo;
@property(nonatomic, strong) NSMutableString *actionLocation;
@property(nonatomic) double locationLatitude;
@property(nonatomic) double locationLongitude;
@property(nonatomic) NSString *type;
@property(nonatomic) long participantCount;
@property(nonatomic, strong) NSMutableString *country;
@property(nonatomic, strong) NSMutableString *town;
@property(nonatomic, strong) PFRelation *members;
@property(nonatomic, strong) Member *organizer;
@property(nonatomic, strong) NSString *telephone;
@property(nonatomic, strong) NSString *email;


- (instancetype) initActionWithName: (NSMutableString *) name
                        description: (NSMutableString *) description
                    actionImageFile: (PFFile *) actionImageFile
                           dateFrom: (NSDate *) dateFrom
                             dateTo: (NSDate *) dateTo
                     actionLocation: (NSMutableString *) actionLocation
                  locactionLatitude: (double) locLat
                  locationLongitude: (double) locLng
                         actionType: (NSString *) actionType
                   participantCount: (long) count
                            country: (NSMutableString *) country
                               town: (NSMutableString *) town
                          organizer: (Member *) organizer
                          telephone: (NSString *)telephone
                              email: (NSString *)email;

- (void) addActionPaticipant: (Member *)member;

+ (NSString *) parseClassName;

- (int)actionTypeInt;

@end
