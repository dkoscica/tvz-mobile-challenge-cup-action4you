//
//  Action.m
//  Action4You
//
//  Created by Bruno Grizli on 4/11/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "Action.h"
#import "Parse/PFObject+Subclass.h"

@implementation Action

@dynamic name;
@dynamic actionDesc;
@dynamic actionImageFile;
@dynamic organizer;
@dynamic dateFrom;
@dynamic dateTo;
@dynamic actionLocation;
@dynamic locationLatitude;
@dynamic locationLongitude;
@dynamic type;
@dynamic participantCount;
@dynamic country;
@dynamic town;
@dynamic members;
@dynamic telephone;
@dynamic email;

#pragma mark - Init methods

- (instancetype) initActionWithName: (NSMutableString *)name
                        description: (NSMutableString *)description
                    actionImageFile: (PFFile *)actionImageFile
                           dateFrom: (NSDate *)dateFrom
                             dateTo: (NSDate *) dateTo
                     actionLocation: (NSMutableString *) actionLocation
                  locactionLatitude: (double) locLat
                  locationLongitude: (double) locLng
                         actionType: (NSString *) type
                   participantCount: (long) count
                            country: (NSMutableString *) country
                               town: (NSMutableString *) town
                          organizer: (Member *)organizer
                          telephone: (NSString *)telephone
                              email: (NSString *)email {
    
    self = [self init];
    if(self) {
        self.name = name;
        self.actionDesc = description;
        self.dateFrom = dateFrom;
        self.dateTo = dateTo;
        self.actionLocation = actionLocation;
        self.locationLatitude = locLat;
        self.locationLongitude = locLng;
        self.type = type;
        self.participantCount = count;
        self.country = country;
        self.town = town;
        self.organizer = organizer;
        self.telephone = telephone;
        self.email = email;
    }
    return self;
}

- (void) addActionPaticipant:(Member *)member {
    PFRelation *membersRelation = [self relationForKey:@"membersRelation"];
    [membersRelation addObject:member];
    [self saveInBackground];
}

//Bruno TODO
- (long) participantCount {
    PFRelation *membersRelation = [self relationForKey:@"membersRelation"];
    long count = [[membersRelation query] countObjects];
    return count;
}

- (int)actionTypeInt {

    if([self.type isEqualToString: HUMANITARIAN]) {
        return 1;
    } else if ([self.type isEqualToString: EDUCATION]) {
        return 2;
    } else if ([self.type isEqualToString: WORK]) {
        return 3;
    } else if ([self.type isEqualToString: SHARE]) {
        return 4;
    }
    return 0;
}

+ (void) load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return NSLocalizedString(@"ACTION", "");
}
    
@end
