//
//  ActionType.h
//  Action4You
//
//  Created by Bruno Grizli on 4/11/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Parse/Parse.h"

@interface ActionTypes : PFObject<PFSubclassing>

@property(nonatomic, strong) NSString *code;
@property(nonatomic, strong) NSString *name;

- (instancetype) initWithCode: (NSString *)code name: (NSString *)name;
+ (NSString *) parseClassName;

@end
