//
//  ActionType.m
//  Action4You
//
//  Created by Bruno Grizli on 4/11/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "ActionTypes.h"

@implementation ActionTypes

@dynamic name;
@dynamic code;

#pragma mark - Init methods		

- (instancetype) initWithCode: (NSString *)code name: (NSString *)name {
    self = [self init];
    if (self) {
        self.code = code;
        self.name = name;
    }
    return self;
}

+ (void) load {
    [self registerSubclass];
}

+ (NSString *) parseClassName {
    return NSLocalizedString(@"TYPES", "");
}

@end
