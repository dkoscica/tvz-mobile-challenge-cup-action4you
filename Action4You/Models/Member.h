//
//  Member.h
//  Action4You
//
//  Created by Bruno Grizli on 4/11/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Parse/Parse.h"
#import "Action.h"

@interface Member : PFObject<PFSubclassing>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *surname;
@property (nonatomic, strong) NSString *shortBio;
@property (nonatomic, strong) NSData *profilePic;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *town;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *email;

- (instancetype) initWithName: (NSString *)name
                      surname: (NSString *)surname
                     shortBio: (NSString *)shortBio
                   profilePic: (NSData *)profilePic
                      country: (NSString *)country
                         town: (NSString *)town
                     username: (NSString *)username
                        email: (NSString *) email;
- (void) addActionForMember: (Action *)action;
- (BOOL) isMemberParticipantInAction: (Action *)action;
- (NSArray *) getActionsForMember;
+ (NSString *) parseClassName;

@end
