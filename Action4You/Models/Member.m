//
//  Member.m
//  Action4You
//
//  Created by Bruno Grizli on 4/11/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "Member.h"
#import "Parse/PFObject+Subclass.h"

@implementation Member

@dynamic name;
@dynamic surname;
@dynamic shortBio;
@dynamic profilePic;
@dynamic country;
@dynamic town;
@dynamic username;
@dynamic password;
@dynamic email;

#pragma mark - Init methods

- (instancetype) initWithName: (NSString *)name
                      surname: (NSString *)surname
                     shortBio: (NSString *)shortBio
                   profilePic: (NSData *)profilePic
                      country: (NSString *)country
                         town: (NSString *)town
                     username: (NSString *)username
                        email: (NSString *)email{
    self = [self init];
    if (self) {
        self.name = name;
        self.surname = surname;
        self.shortBio = shortBio;
        self.profilePic = profilePic;
        self.country = country;
        self.town = town;
        self.username = username;
        self.email = email;
    }
    return self;
}

- (void) addActionForMember: (Action *)action {
    PFRelation *relation = [self relationForKey: @"memberActions"];
    [relation addObject:action];
    [self save];
}

- (NSArray *) getActionsForMember {
    PFRelation *relation = [self relationForKey: @"memberActions"];
    PFQuery *query = [relation query];
    return [query findObjects];
}

- (BOOL) isMemberParticipantInAction: (Action *)action {
    PFRelation *relation = [self relationForKey: @"memberActions"];
    PFQuery *query = [relation query];
    [query whereKey: @"name" equalTo: action.name];
    if([query countObjects] > 0 ) {
        return YES;
    }
    return NO;
}

+ (void) load {
    [self registerSubclass];
}

+ (NSString *) parseClassName {
    return NSLocalizedString(@"MEMBER", "");
}

@end
