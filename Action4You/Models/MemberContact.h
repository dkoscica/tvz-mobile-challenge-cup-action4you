//
//  MemberContact.h
//  Action4You
//
//  Created by Bruno Grizli on 4/11/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Member.h"
#import "Parse/Parse.h"

@interface MemberContact : PFObject<PFSubclassing>

@property (nonatomic, strong) NSString *contact;
@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) Member *owner;
@property (nonatomic, strong) NSString *contactType;
@property (nonatomic) BOOL isSocial;

- (instancetype) initWithContact: (NSString *)contact
                            link: (NSString *)link
                           owner: (Member *)owner
                     contactType: (NSString *)contactType
                        isSocial: (BOOL)isSocial;
+ (NSString *) parseClassName;
@end
