//
//  MemberContact.m
//  Action4You
//
//  Created by Bruno Grizli on 4/11/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "MemberContact.h"

@implementation MemberContact

@dynamic contactType;
@dynamic contact;
@dynamic link;
@dynamic owner;
@dynamic isSocial;

#pragma mark - Init methods

- (instancetype) initWithContact: (NSString *)contact
                            link: (NSString *)link
                           owner: (Member *)owner
                     contactType: (NSString *)contactType
                        isSocial: (BOOL)isSocial {
    self = [self init];
    if (self) {
        self.contact = contact;
        self.link = link;
        self.owner = owner;
        self.contactType = contactType;
        self.isSocial = isSocial;
    }
    return self;
}

+ (void) load {
    [self registerSubclass];
}

+ (NSString *) parseClassName {
    return NSLocalizedString(@"MEMBER_CONTACT", "");
}

@end
