//
//  Action4YouTableViewCellProtocol.h
//  Action4You
//
//  Created by Dominik Koscica on 25/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ActionTableViewCellProtocol <NSObject>

- (void)didClickOnCellAtIndex:(NSInteger)cellIndex withData:(int)tag;

@end
