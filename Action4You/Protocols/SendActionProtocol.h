//
//  SendActionProtocol.h
//  Action4You
//
//  Created by Dominik Koscica on 26/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SendActionProtocol <NSObject>

- (void)sendActionLocation:(NSMutableString *)actionLocation : (double)latitude : (double)longitude;

@end
