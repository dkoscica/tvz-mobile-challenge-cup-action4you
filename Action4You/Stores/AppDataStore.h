//
//  AppDataStore.h
//  Action4You
//
//  Created by Bruno Grizli on 4/26/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Member.h"

@interface AppDataStore : NSObject {
    Member *currentMember;
}
@property (nonatomic, retain) Member *currentMember;

+ (instancetype) sharedManager;
@end
