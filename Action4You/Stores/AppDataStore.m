//
//  AppDataStore.m
//  Action4You
//
//  Created by Bruno Grizli on 4/26/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "AppDataStore.h"

@implementation AppDataStore

@synthesize currentMember;

+ (instancetype) sharedManager {
    static AppDataStore *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}
@end
