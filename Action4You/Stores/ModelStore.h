//
//  ModelStore.h
//  TVZ-Mc2-App
//
//  Created by Dominik Koscica on 14/03/15.
//  Copyright (c) 2015 Dominik Koscica. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Realm/Realm.h"

@interface ModelStore : NSObject

+ (instancetype)sharedStore;

- (void)addModel:(RLMObject *)model;

- (void)removeModel:(RLMObject *)model;

- (void)removeAllModels;

- (void)updateModel:(RLMObject *)model;

//- (RLMObject *)getModel:(NSInteger *)modelId;

- (RLMResults *)getAllModels;

@end
