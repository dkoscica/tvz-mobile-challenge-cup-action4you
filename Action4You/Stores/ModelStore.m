//
//  ModelStore.m
//  TVZ-Mc2-App
//
//  Created by Dominik Koscica on 14/03/15.
//  Copyright (c) 2015 Dominik Koscica. All rights reserved.
//

#import "ModelStore.h"
#import "Constants.h"

@interface ModelStore ()

@property (atomic, strong) RLMRealm *realm;

@end

@implementation ModelStore

#
#pragma mark - Init methods
#
+ (instancetype)sharedStore {
    
    static ModelStore *sharedStore;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedStore = [[self alloc] initPrivate];
    });
    
    return sharedStore;
}

- (instancetype)init {
    [NSException raise:@"Singleton" format:@"Use + [Model sharedStore]"];
    return nil;
}

- (instancetype)initPrivate {
    
    self = [super init];
    if (self) {
        _realm = [RLMRealm defaultRealm];
        //_dictionary = [NSDictionary new];
    }
    return self;
}

#
#pragma mark - Data manipulation methods
#
- (void)addModel:(RLMObject *)model {
    [_realm beginWriteTransaction];
    [_realm addObject:model];
    [_realm commitWriteTransaction];
}

- (void)removeModel:(RLMObject *)model {
    [_realm beginWriteTransaction];
    [_realm deleteObject:model];
    [_realm commitWriteTransaction];
}

- (void)removeAllModels {
    [_realm beginWriteTransaction];
    [_realm deleteAllObjects];
    [_realm commitWriteTransaction];
}

- (void)updateModel:(RLMObject *)model {
    [_realm beginWriteTransaction];
    [RLMObject createOrUpdateInRealm:_realm withObject:model];
    [_realm commitWriteTransaction];
}

//- (RLMObject *)getModel:(NSInteger *)modelId {
//    
//}

- (RLMResults *)getAllModels{
    return [RLMObject allObjects];
    //return [model allObjects];
}

@end
