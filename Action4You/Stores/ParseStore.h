//
//  ParseStore.h
//  Action4You
//
//  Created by Bruno Grizli on 4/11/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Parse/Parse.h"
#import "Action.h"
#import "Member.h"

@interface ParseStore : NSObject

+ (void)saveAction:(Action *)action imageView:(UIImageView *)imageView;
+ (void)pinCurrentUser: (PFUser *)use;
+ (BOOL)isCurrentMemberParticipantInAction: (Action *) action;

@end
