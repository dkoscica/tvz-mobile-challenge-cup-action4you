//
//  ParseStore.m
//  Action4You
//
//  Created by Bruno Grizli on 4/11/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "ParseStore.h"
#import "AppDataStore.h"

@implementation ParseStore

+ (void)saveAction:(Action *)action imageView:(UIImageView *)imageView {
//// Convert to JPEG with 50% quality
//NSData* data = UIImageJPEGRepresentation(imageView.image, 0.5f);
//PFFile *imageFile = [PFFile fileWithName:@"Image.jpg" data:data];
//
//// Save the image to Parse
//
//[imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//    if (!error) {
//        // The image has now been uploaded to Parse. Associate it with a new object
//        Action *saction = [Action new];
//        [saction initActionWithName:action.name
//                       description:action.description
//                   actionImageData:imageFile
//                         organizer:nil
//                          dateFrom:nil
//                            dateTo:nil
//                    actionLocation:action.address.text
//                 locactionLatitude:15.6
//                 locationLongitude:16.2
//                        actionType:action.title
//                  participantCount:0
//                           country:action.address.text
//                              town:action.address.text];
//        [saction setObject:imageFile forKey:@"actionImageData"];
//        
//        //            PFObject* newPhotoObject = [PFObject objectWithClassName:@"PhotoObject"];
//        //            [newPhotoObject setObject:imageFile forKey:@"image"];
//        
//        [saction saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//            if (!error) {
//                NSLog(@"Saved");
//            }
//            else{
//                // Error
//                NSLog(@"Error: %@ %@", error, [error userInfo]);
//            }
//        }];
//    }
//}];
}

+ (void) pinCurrentUser:(PFUser *)user {
    PFQuery *query = [Member query];
    [query whereKey: @"username" equalTo: user.username];
     NSArray *objects = [query findObjects];
     if (objects && [objects count] > 0) {
        Member *member = [objects firstObject];
        [member pinWithName: @"CurrentMember"];
        [AppDataStore sharedManager].currentMember = member;
    }
}

+ (BOOL) isCurrentMemberParticipantInAction: (Action *) action {
    Member *currentMember = [AppDataStore sharedManager].currentMember;
    return [currentMember isMemberParticipantInAction:action];
}

@end
