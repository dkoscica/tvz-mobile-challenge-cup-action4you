//
//  Action4YouImageView.h
//  Action4You
//
//  Created by Dominik Koscica on 17/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Action4YouImageView : UIImageView

@end
