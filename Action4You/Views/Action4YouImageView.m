//
//  Action4YouImageView.m
//  Action4You
//
//  Created by Dominik Koscica on 17/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "Action4YouImageView.h"
#import "Constants.h"
#import "UIColor+Action4YouColors.h"

@implementation Action4YouImageView

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.clipsToBounds = YES;
        self.layer.cornerRadius = CORNER_RADIUS;
        self.layer.borderColor = [[UIColor whiteColor] CGColor];
        self.layer.borderWidth = BORDER_WIDTH;
        self.backgroundColor = [UIColor action4YouTransparentGrayColor];
    }
    return self;
}

@end
