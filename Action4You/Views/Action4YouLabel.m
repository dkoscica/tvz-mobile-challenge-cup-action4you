//
//  Action4YouLabel.m
//  Action4You
//
//  Created by Dominik Koscica on 17/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "Action4YouLabel.h"
#import "Constants.h"
#import "UIColor+Action4YouColors.h"

@implementation Action4YouLabel


- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.clipsToBounds = YES;
        
        //Font and text
        self.font = [UIFont fontWithName:APP_FONT size:NORMAL_TEXT_SIZE];
        self.minimumScaleFactor = NORMAL_MIN_TEXT_SIZE;
        self.adjustsFontSizeToFitWidth = YES;
        self.textColor = [UIColor whiteColor];
        
        //Border
        self.layer.borderColor = [[UIColor whiteColor] CGColor];
        self.layer.borderWidth = BORDER_WIDTH;
        self.layer.cornerRadius = CORNER_RADIUS;
        
        //Size
        CGRect frameRect = self.frame;
        frameRect.size.height = VIEW_HEIGHT;
        self.frame = frameRect;
        
        self.backgroundColor = [UIColor action4YouTransparentGrayColor];
    }
    return self;
}

//Added Label left margin
- (void)drawTextInRect:(CGRect)rect {
    UIEdgeInsets insets = {0, LEFT_MARGIN, 0, 0};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

@end
