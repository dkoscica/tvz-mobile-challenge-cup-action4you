//
//  Action4YouTextField.h
//  Action4You
//
//  Created by Dominik Koscica on 15/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Action4YouTextField : UITextField

@end
