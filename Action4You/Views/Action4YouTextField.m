//
//  Action4YouTextField.m
//  Action4You
//
//  Created by Dominik Koscica on 15/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "Action4YouTextField.h"
#import "Constants.h"
#import "UIColor+Action4YouColors.h"

@implementation Action4YouTextField


- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.clipsToBounds = YES;
    
        //Font and text
        self.font = [UIFont fontWithName:APP_FONT size:NORMAL_TEXT_SIZE];
        self.textColor = [UIColor whiteColor];
    
        //Border
        self.borderStyle = UITextBorderStyleRoundedRect;
        self.layer.borderColor = [[UIColor whiteColor] CGColor];
        self.layer.borderWidth = BORDER_WIDTH;
        self.layer.cornerRadius = CORNER_RADIUS;
    
        //Size
        CGRect frameRect = self.frame;
        frameRect.size.height = VIEW_HEIGHT;
        self.frame = frameRect;
    
        self.backgroundColor = [UIColor action4YouTransparentGrayColor];
        self.returnKeyType = UIReturnKeyDone;
    }
    return self;
}

- (void) drawPlaceholderInRect:(CGRect)rect {
    [[UIColor whiteColor] setFill];
    CGRect placeholderRect = CGRectMake(rect.origin.x, (rect.size.height - self.font.pointSize)/2 - 3, rect.size.width, self.font.pointSize);
    [[self placeholder] drawInRect:placeholderRect withFont:[UIFont systemFontOfSize:NORMAL_TEXT_SIZE] lineBreakMode:NSLineBreakByWordWrapping alignment:self.textAlignment];
}

@end
