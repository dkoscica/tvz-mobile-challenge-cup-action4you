//
//  Action4YouTextView.m
//  Action4You
//
//  Created by Dominik Koscica on 15/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "Action4YouTextView.h"
#import "Constants.h"
#import "UIColor+Action4YouColors.h"

@implementation Action4YouTextView

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        //Font and text
        self.font = [UIFont fontWithName:APP_FONT size:NORMAL_TEXT_SIZE];
        self.textColor = [UIColor whiteColor];
    
        //Border
        self.layer.cornerRadius = CORNER_RADIUS;
        self.layer.borderColor = [[UIColor whiteColor] CGColor];
        self.layer.borderWidth = BORDER_WIDTH;

        self.backgroundColor = [UIColor action4YouTransparentGrayColor];
    }
    return self;
}

//Added TextField text left margin
- (CGRect) textRectForBounds:(CGRect)bounds {
    return CGRectMake(bounds.origin.x + LEFT_MARGIN, bounds.origin.y + 8,
                      bounds.size.width - 40, bounds.size.height - 16);
}


@end
