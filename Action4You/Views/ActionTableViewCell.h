//
//  ActionTableViewCell.h
//  Action4You
//
//  Created by Dominik Košćica on 11/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActionTableViewCellProtocol.h"

@interface ActionTableViewCell : UITableViewCell <ActionTableViewCellProtocol>

@property (nonatomic, weak) id<ActionTableViewCellProtocol>delegate;
@property (nonatomic) NSInteger cellIndex;

@property (nonatomic, strong) IBOutlet UIImageView *actionImage;
@property (nonatomic, strong) IBOutlet UILabel *actionName;
@property (nonatomic, strong) IBOutlet UILabel *actionAddress;
@property (nonatomic, strong) IBOutlet UILabel *numberOfParticipants;
@property (nonatomic, strong) IBOutlet UILabel *actionTime;
@property (nonatomic, strong) IBOutlet UIView *tableViewCellContainer;
@property (nonatomic, strong) IBOutlet UIButton *actionLocationButton;
@property (nonatomic, strong) IBOutlet UIButton *actionShareButton;

@end
