//
//  ActionTableViewCell.m
//  Action4You
//
//  Created by Dominik Košćica on 11/04/15.
//  Copyright (c) 2015 Team Action4You. All rights reserved.
//

#import "ActionTableViewCell.h"
#import "Constants.h"
#import "UIColor+Action4YouColors.h"
#import "Action.h"

@implementation ActionTableViewCell

- (void)awakeFromNib {
    // Initialization code
    if(self) {
        self.tableViewCellContainer.layer.cornerRadius = CORNER_RADIUS;
        self.tableViewCellContainer.layer.borderColor = [UIColor whiteColor].CGColor;
        self.tableViewCellContainer.layer.borderWidth = BORDER_WIDTH;
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (IBAction)buttonClicked:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didClickOnCellAtIndex:withData:)]) {
        [self.delegate didClickOnCellAtIndex:_cellIndex withData:(int)sender.tag];
    }
}

-(void)didClickOnCellAtIndex:(NSInteger)cellIndex withData:(int)tag{}

@end
